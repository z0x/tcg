# Sofie's TCG coding exercise

 Hi there,
 
 This example makes two very large assumptions:
 
 1) Capitalization and whitespace matters
 2) Name, Grant, and Amount are always the first, second and third element of a (sub?) array
 
 ### Installation
 In the folder root run
  
 ```
 composer.phar dump-autoload -o
 ``` 
 
 ### Comments
 
 I did see the part where you wanted "one page readable code" 
 but I couldn't make a 200+ line file look readable.

This is _annotated_ not documented. Noramlly i'd be writing PHPdoc blocks

### Answer to \#2

[Datatables](https://www.datatables.net/) is my goto JS library for sorting html tables. Its pretty fantastic. Lots of flexability, 
can add functionality to a regular html table, or can take JSON and build your tables that way.

Only drawback is that it is a jquery plugin.