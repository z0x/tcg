<?php

namespace z0x\tcg;

class TCGExercise{
    public $headings = [];  //an array for the table headers;
    public $grant_arr = [] ; //an array to hold the grant data

    private $pg; //ProcessGrants Class
    private $html_builder; //HTMLTableBuilder Class

    public $html; //eventual output


    function run(){
        $this->pg = new ProcessGrants($this->grant_arr);

        $this->html_builder = new HTMLTableBuilder();
        $this->grant_arr= $this->pg->process();                 //kick off the grant processing

        $this->html_builder->activities = $this->pg->activities;//set the appropriate vars
        $this->html_builder->grants = $this->pg->grant_arr_proc;

        $this->html = $this->html_builder->build();

    }

}