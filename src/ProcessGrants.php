<?php
namespace z0x\tcg;

class ProcessGrants{
    public $grant_arr; //array to hold the original grant data
    public $grant_arr_proc; // array to hold processed grant data
    public $activities = [];
    private $institutions = [];
    function __construct(array $grant_arr = [])
    {
        $this->grant_arr = $grant_arr;
    }

    public function process(){
        $this->check_data_exists();
        $this->extract_institutions();
        $this->extract_activities();
        $this->calc_institute_contrib();

        return [$this->activities,$this->grant_arr_proc];

    }

    private function calc_institute_contrib(){          //a little nutty but the best & fastest way to accomplish this i think.
        foreach ($this->institutions as $institute){    //take an institution
            foreach ($this->activities as $activity){   //and an activity
                $activity_contib = 0;                   //reset the total contribution the institution has made to that activity
                $grant_index=0;                         //and the position in the grant array.
                foreach ($this->grant_arr as $grant){
                    if(!array_key_exists(0,$grant)      //multi
                       || !array_key_exists(1,$grant)   //line
                       || !array_key_exists(2,$grant)   //logic (for readability)
                    ){                                      //to ignore incomplete data sets
                    $grant_index++;
                        continue ;
                    }if($institute === $grant[0] && $activity === $grant[1]){ //match the activity to the grant
                        $activity_contib+= $grant[2];       //add up the total contribution to the activity from the
                        unset($this->grant_arr[$grant_index]);//and remove the "good" data, so at the end we're only left with "bad" data. Handling that isn't in the spec, but the option is there.
                    }
                    $grant_index++;
                }
                $this->grant_arr_proc[$institute][$activity] = $activity_contib;   // finally add an array populated with the total contribution of an institution to an activity.
            }

        }
    }


    /*
     * these next two methods could be optimized but i'm out of time.
     * */
    private function extract_activities(){                                          //same logic as extract_institutions
        foreach ($this->grant_arr as $grant){
            if(array_key_exists(1,$grant)){
                array_push($this->activities, $grant[1]);
            }
        }

        $this->activities = array_unique($this->activities, SORT_STRING);  //remove any duplicates. Sort alphabetically
        $this->activities = array_values($this->activities);                        //and make sure we're zero-indexed and enumerated correctly.
    }

    private function extract_institutions(){
        foreach ($this->grant_arr as $grant){                                       //loop through the grant array
            if(array_key_exists(0,$grant)){                                                   //assuming the institution is in the first element
                array_push($this->institutions, $grant[0]);                  //push the name of the institute into a new array
            }
        }

        $this->institutions = array_unique($this->institutions, SORT_STRING);//remove any duplicates. Sort alphabetically
        asort($this->institutions,  SORT_NATURAL); //sort them;
        $this->institutions = array_values($this->institutions);                     //and make sure we're zero-indexed and enumerated correctly.

    }

    private function check_data_exists(){
        try {
            if ($this->grant_arr === []) {
                throw new \ErrorException("Grant data not set"); /*honestly not sure how to handle errors in a coding exercise*/
            }
        }catch (\ErrorException $e){
            print "Error: " . $e->getMessage() . "\n";
        }
    }
}