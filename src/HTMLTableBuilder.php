<?php
namespace z0x\tcg;

class HTMLTableBuilder{
    public $activities;
    public $grants;

    function __construct(array $activities= [], array $grants = [])
    {
        $this->activities = $activities;
        $this->grants = $grants;
    }

    public function build(){
        $this->check_data_exists();
        $table = $this->array_to_html();

        //super barebones template. i'd never do it like this live.
        $html = <<<html
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
</html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
$table
</html>
html;

        return $html;
    }


    public function array_to_html(){    //if you have strong opinions on heredocs, my apologies. I once worked at a pearl shop.
        $t_head="<td> </td>";           //create an empty first cell in the header.
        $t_rows="";

        foreach($this->activities as $activity){                              //grab an institute
            $t_head .= <<<html
<td>$activity</td>
html;
        }

        foreach($this->grants as $institute=>$grant_activity){
            $activity_count = 0;
            //$institute = $grant[0];
            $t_rows.="<tr><td>$institute</td>";                               //put in in the row

            foreach ($this->activities as $activity){                         //grab an activity
                if(array_key_exists($activity,$this->grants[$institute])){    //match the institute and activity in the grant property
                   $contrib = $grant_activity[$activity];                     //find the contribution
                    $t_rows .= "<td>$contrib</td>";                           //and add it to the table.
               } else{
                    $t_rows.= "<td>0</td>>";                                  //zero for no data
                }

               $activity_count++;
            }
            $t_rows.="</tr>";
        }

        $table_html=<<<html
<table>
    <thead>
        <tr>
            $t_head
        </tr>
    </thead>
    $t_rows
</table>
html;

    return $table_html;

    }

    private function check_data_exists(){ //again, not sure how to handle errors in this project.
        try {
            if ($this->activities === []) {
                throw  new \ErrorException("Grant activities not set. Check grant array format.");
            }
        }catch (\ErrorException $e){
            print "Error: " . $e->getMessage() . "\n";
        }
    }


}