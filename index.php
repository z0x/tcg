<?php

include ("vendor/autoload.php");

use z0x\tcg\TCGExercise;

$tcg = new TCGExercise();


$tcg->grant_arr = [
    ["UCSD","Fusion Research","1000"],
    ["UCSD","Genetic Research","130"],
    ["Harvard","Fusion Research","2000"],
    ["Harvard","Genetic Research","30"],
    ["Rutgers","Mathematics", "200"],
    ["Rutgers","Fusion Research", "100"]
    ];

 $tcg->run();

 echo $tcg->html;